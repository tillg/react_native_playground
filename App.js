import React from 'react';
import {AppRegistry, Button, Text, TextInput, View} from 'react-native';
import {StackNavigator} from 'react-navigation';

// Using a library taken from here:
// https://github.com/satya164/react-navigation-addons
import {enhance} from './index'

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Home'
  };

  constructor(props) {
    super(props)
    this.state = {
      user: 'Lucy'
    };
  }

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Text>This is teh Home Screen</Text>
        <Button
          onPress={() => navigate('View', {user: this.state.user})}
          title={"See data of " + this.state.user}/>
      </View>
    );
  }
}

class ViewScreen extends React.Component {
  // Nav options can be defined as a function of the screen's props:
  static navigationOptions = ({navigation}) => ({title: `View ${navigation.state.params.user}`, headerRight: <Button
    title="Edit"
    onPress={() => navigation.navigate('Edit', {user: navigation.state.params.user})}/>});
  render() {
    // The screen's current route is passed in to `props.navigation.state`:
    const {params} = this.props.navigation.state;
    return (
      <View>
        <Text>Her name is {params.user}</Text>
      </View>
    );
  }
}

class EditScreen extends React.Component {
  // Nav options can be defined as a function of the screen's props:
  static navigationOptions = ({navigation}) => ({title: `Edit ${navigation.state.params.user}`, headerRight: <Button
    title="Save"
    onPress={() => navigation.navigate('Edit', {user: navigation.state.params.user})}/>});

  constructor(props) {
    super(props)
    console.log('EditScreen.constructor: Entering')
    this.state = {
      user: props.navigation.state.params.user
    }
  }

  componentWillMount() {
    console.log('EditScreen.componentWillMount: Entering')
    this
      .props
      .navigation
      .setOptions({headerTitle: this.props.navigation.state.params.user, headerLeft: (<Button title='Save' onPress={this._handleSave}/>)});
  }

  render() {
    console.log('EditScreen.render: Entering')

    return (
      <View>
        <Text>Now here name is {this.state.user}</Text>
        <TextInput
          style={{
          height: 50,
          borderColor: 'gray',
          borderWidth: 1
        }}
          value={this.state.user}
          onChangeText={(text) => this.setState({user: text})}
          editable={true}/>
      </View>
    );
  }

  componentDidMount() {
    console.log('EditScreen.componentDidMount: Entering')
  }
}

const SimpleApp = StackNavigator({
  Home: {
    screen: HomeScreen
  },
  View: {
    screen: ViewScreen
  },
  Edit: {
    screen: EditScreen
  }

})

AppRegistry.registerComponent('react_native_playground', () => SimpleApp);