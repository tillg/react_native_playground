# react_native_playground

A playground for playing around w/o breaking my _real projects_.

## Navigation

Trying to have a navigation like i need it in Spotz2:

* A Home screen (that would for example contain a list of items)
* A View screen (i.e. show the data of the one selcted Spotz)
* An edit screen (alows to edit a Spotz)

The question I try to sort out: How to pass data from one screen to another?

Passing it to the screen _below_ (i.e. on top of the navigation stack) is easy: passed props become available under this.props.navigation.state. But how can we send the (potentially modified) data back to the parent view - especially when users are supposed to click a button in the navigation menu.

The problem is that the navigation menu is a static attribute to the screen and therefore cannot simply access the member variable (i.e. state) of the screen.

## react-navigation-addons

This library promises to solve this accessibility problem generically... So I try to play around with it.
